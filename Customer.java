package com.emids.insurance.beans;

public class Customer {
	private String name;
	private String gender;
	private Integer age;
	private Habits habits;
	private HealthCondition condition;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Habits getHabits() {
		return habits;
	}

	public void setHabits(Habits habits) {
		this.habits = habits;
	}

	public HealthCondition getCondition() {
		return condition;
	}

	public void setCondition(HealthCondition condition) {
		this.condition = condition;
	}

}
