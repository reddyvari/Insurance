package com.emids.insurance.beans;

public class Habits {
	private Boolean smoke;
	private Boolean alcohol;
	private Boolean dailyExercise;
	private Boolean drugs;

	public Boolean getSmoke() {
		return smoke;
	}

	public void setSmoke(Boolean smoke) {
		this.smoke = smoke;
	}

	public Boolean getAlcohol() {
		return alcohol;
	}

	public void setAlcohol(Boolean alcohol) {
		this.alcohol = alcohol;
	}

	public Boolean getDailyExercise() {
		return dailyExercise;
	}

	public void setDailyExercise(Boolean dailyExercise) {
		this.dailyExercise = dailyExercise;
	}

	public Boolean getDrugs() {
		return drugs;
	}

	public void setDrugs(Boolean drugs) {
		this.drugs = drugs;
	}

}
