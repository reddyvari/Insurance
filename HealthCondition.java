package com.emids.insurance.beans;

public class HealthCondition {
	private Boolean bp;
	private Boolean sugar;
	private Boolean overWeight;
	private Boolean hyperTension;
	public Boolean getBp() {
		return bp;
	}
	public void setBp(Boolean bp) {
		this.bp = bp;
	}
	public Boolean getSugar() {
		return sugar;
	}
	public void setSugar(Boolean sugar) {
		this.sugar = sugar;
	}
	public Boolean getOverWeight() {
		return overWeight;
	}
	public void setOverWeight(Boolean overWeight) {
		this.overWeight = overWeight;
	}
	public Boolean getHyperTension() {
		return hyperTension;
	}
	public void setHyperTension(Boolean hyperTension) {
		this.hyperTension = hyperTension;
	}
	
	
}
