package com.emids.main;

import com.emids.insurance.beans.Customer;
import com.emids.insurance.beans.Habits;
import com.emids.insurance.beans.HealthCondition;

public class PremiumCalculator {

	String name;
	String gender;
	Integer age;
	Habits habits;
	HealthCondition condition;
	static Integer premium = 5000;

	public PremiumCalculator(Customer customer) {
		if (customer != null) {
			name = customer.getName();
			gender = customer.getGender();
			age = customer.getAge();
			habits = customer.getHabits();
			condition = customer.getCondition();
		}
	}

	private String getTemplate(String name) {
		return "Health Insurance Premium for " + name + " :";
	}

	
	private String calculatePremium(Customer customer) {
		if (age < 18) {
			return getTemplate(name) + premium;
		}

		if ((age >= 25 && age <= 30)) {
			premium = premium + (10 * premium / 100);
		}

		if (gender.equals("Male")) {
			premium = premium + (2 * premium / 100);
		}
		if (condition.getBp() || condition.getHyperTension() || condition.getSugar()) {
			premium = premium + (1 * premium / 100);
		}

		if (habits.getDrugs() || habits.getSmoke() || habits.getAlcohol()) {
			premium = premium + (3 * premium / 100);
		} else if (habits.getDailyExercise()) {
			premium = premium - (3 * premium / 100);
		}

		/*
		 * Base premium for anyone below the age of 18 years = Rs. 5,000 % increase
		 * based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10%
		 * | 40+ -> 20% increase every 5 years Gender rule: Male vs female vs Other % ->
		 * Increase 2% over standard slab for Males Pre-existing conditions
		 * (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1%
		 * per condition Habits
		 * 
		 * 
		 * Good habits (Daily exercise) -> Reduce 3% for every good habit Bad habits
		 * (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit
		 */

		return getTemplate(name) + premium;
	}
}
