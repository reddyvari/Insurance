package com.emids.test;

import com.emids.insurance.beans.Customer;
import com.emids.main.PremiumCalculator;

public class TestPremium {
	Customer c = new Customer();
//add customer stuff
	PremiumCalculator p = new PremiumCalculator(c);

	@Test
	public void testSalutationMessage() {
		assertEquals(6856, p.calculatePremium(c));
	}
}
